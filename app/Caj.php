<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caj extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Caj';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Nazov_caju', 'Varka', 'Popis', 'distributorID' ];

}
