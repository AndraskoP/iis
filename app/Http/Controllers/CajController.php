<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Caj;

class CajController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $caj = Caj::get();

      return view('caj.index')->with('caj', $caj);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('caj.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // return $request->all();

      $caj =  Caj::create([
          'Nazov_caju' => $request['Nazov_caju'],
          'Varka' => $request['Varka'],
          'Popis' => $request['Popis'],
          'distributorID' => $request['distributorID'],
          'created_at' => $request['created_at'],
          'updated_at' => $request['updated_at'],
      ]);

      return redirect('caj');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $caj = Caj::findOrFail($id);
        return view('caj.edit', compact('caj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $caj = Caj::findOrFail($id);
      $caj->update($request->all());
      return redirect('caj');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $caj = Caj::findOrFail($id);

      $caj->delete($caj);

      return redirect('caj');
    }
}
