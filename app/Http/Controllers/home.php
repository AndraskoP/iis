<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class home extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()         //index. prva stranka 
    {
        //
        return view('home');
    }


    public function login(Request $request)
    {
        //
        $user = DB::table('Administrator')
                ->select('Administrator.login', 'Administrator.heslo')
                ->where('Administrator.login', '=', $_POST["user"])
                ->where('Administrator.heslo', '=', $_POST["pass"])
                ->get();

        if (empty($user))
            echo 'pouzivatel nenajdeny';
        else
            return view('admin');
    }

    public function register()
    {
        return view('register');
    }
}
