<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Oblast extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Oblast', function(Blueprint $table){
            //$table->increments('id');
            $table->string('Skratka_statu', 5);
            $table->char('Popis_oblasti', 200);
            $table->integer('cajID')->unsigned(); //kazda Oblast musi mat ID nejakeho caju -> oblast bez caju nemoze existovat
            });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
