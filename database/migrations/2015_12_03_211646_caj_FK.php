<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CajFK extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('Caj', function(Blueprint $table){//pridavam FK do tabulky CAJ 

            $table->foreign('distributorID')->references('id')->on('Distributor'); //v tabulke caj najdi stplec distributorID, ten sa odkazuje na ID v tabluke Distrubutor
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
