<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Odberatel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Odberatel', function(Blueprint $table) {
            //$table->increments('id');   //cislo preukazu
            //$table->integer('id');
            //$table->integer('id', false);
            $table->string('Meno', 30);
            $table->string('Priezvisko', 30);
            $table->string('Email', 30);
            $table->integer('cislo', 8);
            $table->string('login', 15);
            $table->string('heslo', 20);
           
            });
        
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
