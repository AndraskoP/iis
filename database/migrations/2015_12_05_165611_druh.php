<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Druh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Druh', function(Blueprint $table){
            //$table->increments('id');
            $table->string('Doba_luhovania', 15);
            $table->char('Odporucana_teplota', 50);
            $table->integer('cajID')->unsigned(); //kazda Oblast musi mat ID nejakeho caju -> oblast bez caju nemoze existovat
            });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
