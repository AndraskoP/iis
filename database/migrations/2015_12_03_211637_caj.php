<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Caj extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Caj', function(Blueprint $table){
            $table->increments('id', compact('autoIncrement', 'unsigned'));
            $table->string('Nazov_caju', 30);
            $table->integer('Varka')->unsigned();
            $table->char('Popis', 200);
            $table->integer('distributorID')->unsigned();
            $table->timestamps();
            });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
