<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OblastFKCaj extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('Oblast', function(Blueprint $table){//pridavam FK do tabulky CAJ 

            $table->foreign('cajID')->references('id')->on('Caj'); //v tabulke caj najdi stplec distributorID, ten sa odkazuje na ID v tabluke Distrubutor
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
