<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Objednavka extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Objednavka', function(Blueprint $table){
            $table->increments('id');   //cislo preukazu
            $table->string('Meno_odberatela', 30);
            $table->string('Priezvisko_odberatela', 30);
            $table->string('Email', 30);
            $table->integer('odberatelID')->unsigned();
            });
        
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
