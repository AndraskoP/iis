<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Distributor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Distributor', function(Blueprint $table){
            $table->increments('id');   //cislo preukazu
            $table->string('meno', 30);
            $table->string('priezv', 30);
            $table->string('rodne_cislo', 10);
            $table->string('pozicia', 200);
            $table->string('heslo', 10);
            });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
