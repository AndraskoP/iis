<?php

use Illuminate\Database\Seeder;

class DruhSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Druh')->insert([
            "Doba_luhovania" => "10 min.",
            "Odporucana_teplota" => "Min. 90°",
            "cajID" => "1"
            ]);

        DB::table('Druh')->insert([
            "Doba_luhovania" => "15 min.",
            "Odporucana_teplota" => "Min. 95°",
            "cajID" => "2"
            ]);
        
        DB::table('Druh')->insert([
            "Doba_luhovania" => "5 min.",
            "Odporucana_teplota" => "Min. 80°",
            "cajID" => "3"
            ]);
    }
}
