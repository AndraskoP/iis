<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         $this->call(DistributorSeeder::class);
         $this->call(CajSeeder::class);
         $this->call(AdministratorSeeder::class);
         $this->call(OblastSeeder::class);
         $this->call(DruhSeeder::class);
         $this->call(OdberatelSeeder::class);

        Model::reguard();
    }
}
