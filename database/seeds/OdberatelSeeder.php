<?php

use Illuminate\Database\Seeder;

class OdberatelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Odberatel')->insert([
            "Meno" => "Martin",
            "Priezvisko" => "Laktos",
            "Email" => "laktos@gmail.com", 
            "cislo" => "789521458",
            "login" => "laktos",
            "heslo" => "laktos123"
            ]);
    }
}