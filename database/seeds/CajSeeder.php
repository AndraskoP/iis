<?php

use Illuminate\Database\Seeder;


class CajSeeder extends Seeder {

    public function run()
    {

        DB::table('Caj')->insert([
            "Nazov_caju" => "Earl Grey",
            "Varka" => "1",
            "Popis" => "Paradny cierny",
            "distributorID" => "1"
            ]);
        
        DB::table('Caj')->insert([
            "Nazov_caju" => "Zeleny caj",
            "Varka" => "1",
            "Popis" => "Zo zihlavy",
            "distributorID" => "2"
            ]);

        DB::table('Caj')->insert([
            "Nazov_caju" => "Ovocny",
            "Varka" => "1",
            "Popis" => "Johodovy",
            "distributorID" => "2",
            ]);


       
    }

}