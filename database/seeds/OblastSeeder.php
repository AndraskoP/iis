<?php

use Illuminate\Database\Seeder;

class OblastSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Oblast')->insert([
            "Skratka_statu" => "SVK",
            "Popis_oblasti" => "Paradna, fajna zavlhcena.",
            "cajID" => "1"
            ]);
        
        DB::table('Oblast')->insert([
            "Skratka_statu" => "GER",
            "Popis_oblasti" => "Oblast velmi prinosna prostrediu.",
            "cajID" => "2"
            ]);

        DB::table('Oblast')->insert([
            "Skratka_statu" => "USA",
            "Popis_oblasti" => "Oblastnenstvo nad mieru spokojne.",
            "cajID" => "3",        
            ]);               
    }

}