<?php

use Illuminate\Database\Seeder;


class DistributorSeeder extends Seeder {

    public function run()
    {

        DB::table('Distributor')->insert([
            "meno" => "Anton",
            "priezv" => "Jofko",
            "rodne_cislo" => "9465856257",
            "pozicia" => "distributor",
            "heslo" => "abc",
            ]);
        
        DB::table('Distributor')->insert([
            "meno" => "Michal",
            "priezv" => "Macutek",
            "rodne_cislo" => "9469856257",
            "pozicia" => "distributor",
            "heslo" => "123",
            ]);


        
    }

}