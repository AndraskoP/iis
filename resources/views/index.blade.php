<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <link rel="stylesheet" href="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/css/bootstrap.css">

        {!! Html::style('css/app.css'); !!}

    </head>
    <body>
        <div class="container">
            <div class="content">
                
                <div class="title">@yield('telo')</div>

            </div>
        </div>
    </body>
</html>
