@extends('layouts.master')

@section('content')

    <h1>Caj</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Nazov Caju</th><th>Varka</th><th>Popis</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $caj->id }}</td> <td> {{ $caj->Nazov_caju }} </td><td> {{ $caj->Varka }} </td><td> {{ $caj->Popis }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection