@extends('app')

@section('htmlheader_title')
Caj
@endsection

@section('lietadlo_active')
active
@endsection

@section('contentheader_title')
<h1>Upravit caj</h1>
@endsection

@section('main-content')

    {!! Form::model($caj, [
        'method' => 'PATCH',
        'route' => ['caj.update', $caj->id],
        'class' => 'form-horizontal'
    ]) !!}

                <div class="form-group {{ $errors->has('Nazov_caju') ? 'has-error' : ''}}">
                {!! Form::label('Nazov_caju', 'Nazov Caju: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('Nazov_caju', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('Nazov_caju', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('Varka') ? 'has-error' : ''}}">
                {!! Form::label('Varka', 'Varka: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('Varka', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('Varka', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('Popis') ? 'has-error' : ''}}">
                {!! Form::label('Popis', 'Popis: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('Popis', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('Popis', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('distributorID') ? 'has-error' : ''}}">
                {!! Form::label('distributorID', 'Distributorid: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('distributorID', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('distributorID', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection
