@extends('app')

@section('htmlheader_title')
Caj
@endsection

@section('lietadlo_active')
active
@endsection

@section('contentheader_title')
<h1>Čaj</h1>
@endsection

@section('main-content')

    <a href="{{ route('caj.create') }}" class="btn btn-primary pull-right btn-sm">Add New Caj</a>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th>Nazov Caju</th><th>Varka</th><th>Popis</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($caj as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td><a href="{{ url('/caj', $item->id) }}">{{ $item->Nazov_caju }}</a></td><td>{{ $item->Varka }}</td><td>{{ $item->Popis }}</td>
                    <td>
                        <a href="{{ route('caj.edit', $item->id) }}">
                            <button type="submit" class="btn btn-primary btn-xs">Update</button>
                        </a> /
                        {!! Form::open([
                            'method'=>'DELETE',
                            'route' => ['caj.destroy', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{-- <div class="pagination"> {!! $caj->render() !!} </div> --}}
    </div>

@endsection
