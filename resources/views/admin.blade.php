<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>IS - distribucia cajov</title>
<link rel="stylesheet" href="styles.css" type="text/css" />
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--
anatine, a free CSS web template by ZyPOP (zypopwebtemplates.com/)

Download: http://zypopwebtemplates.com/

License: Creative Commons Attribution
//-->
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
</head>

<body>

		<section id="body" class="width">
			<aside id="sidebar" class="column-left">

			<header>
				<h1><a href="#">Meno prihlaseneho</a></h1>

				<h2>Jeho pozicia</h2>
				
			</header>

			<nav id="mainnav">
  				<ul>
                            		<li class="selected-item"><a href="index.html">Home</a></li>
                           		 <li><a href="examples.html">Čaje</a></li>
                           		 <li><a href="#">Distributori</a></li>
                            		<li><a href="#">Administrátori</a></li>
                            		<li><a href="#">Odhlasit</a></li>
                        	</ul>
			</nav>

			
			
			</aside>
			<section id="content" class="column-right">
                		
	    <article>
				
			
			<h2>Introduction to anatine</h2>
			<div class="article-info">Posted on <time datetime="2013-05-14">14 May</time> by <a href="#" rel="author">Joe Bloggs</a></div>

            <p> <a href="http://zypopwebtemplates.com/" title="ZyPOP">ZyPOP</a>. This responsive template is completely <strong>free</strong> to use permitting a link remains back to  <a href="http://zypopwebtemplates.com/" title="ZyPOP">http://zypopwebtemplates.com/</a>.</p>


            
            <p>This template has been tested in:</p>


            <ul class="styledlist">
                <li>Firefox</li>
                <li>Opera</li>
                <li>IE</li>
 		<li>Safari</li>
                <li>Chrome</li>
            </ul>

		
		</article>
			
			<footer class="clear">
				<p>&copy; 2015 sitename. <a href="http://zypopwebtemplates.com/">Free CSS Templates</a> by ZyPOP</p>
			</footer>

		</section>

		<div class="clear"></div>

	</section>
	

</body>
</html>
